## About Duke Scholars modules
This documentation covers general information about usage of three Duke Scholars Drupal modules developed by DWS, and Duke Scholars Entities module specifically.

#### Duke Scholars integration for Drupal is provided by these three modules:
- **Scholars Entities module** - https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke-scholars-entities. Provides custom entities for Scholars Profiles, Scholars Organizations, and Positions.
  - Defines fields, custom content types and vocabularies,  routes, permissions, basic templates, and admin interface for managing Scholars entities.
  - Designed to work together with the Scholars Fetcher module for profiles import.
  - Drupal 8/9 compatible.
- **Scholars Fetcher module** - https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke-scholars-fetcher. Implements Scholar profiles import and prepares profiles for use in Drupal.
  - Drupal 8/9 compatible.
- **Scholars Display module** - https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke_scholars_display. Displays data on a Scholar Profile node page via React components.
  - Drupal 8/9 compatible.

## Duke Scholars Entities
### Requirements
- Drupal 8 or 9, with core modules Text, Link, Telephone enabled
- Views
- Views bulk operations
### Installation 

Install the Scholars Entities module as you would normally install a contributed module. Recommended way is to use Composer by adding the following to `composer.json`:

```
"repositories": [
  ...
  "duke/duke_scholars_fetcher": {
    "type": "vcs",
    "url": "https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke-scholars-fetcher.git"
  },
  "duke/duke_scholars_entities": {
    "type": "vcs",
    "url": "https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke-scholars-entities.git"
  },
  ...
],    
"require": {
  ...
  "duke/duke_scholars_entities": "^2.0.0",
  "duke/duke_scholars_fetcher": "^2.0.0",
}
```

Alternatively, download a zip archive of the module from the module's [gitlab repo](https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke-scholars-entities) and put it into your site's modules/contrib folder.

### Configuration

1. Enable the Scholars Entities and Fetcher modules.
2. In Drupal Admin, under Content you will find a new menu item Scholars@Duke Entities which contains three new sections for Profiles, Organizations, and Positions.
3. There are two ways of importing a new Scholars Profile to the site:
    - manually by using "Add scholars@duke profile" button
    - adding a new Scholars@duke Organization and checking the box to "Import Profiles". In this case all people that belong to this organization will be automatically imported.

#### To manually add a single Scholars Profile:
- Click "Add scholars@duke profile" button.
- Type the name of the person. Note that this name will be updated with the incoming information from Scholars website during the next import.
- Add person's scholars@duke URI in the format `https://scholars.duke.edu/individual/per12345678`. To get a person's URI, please find the person on [Scholars website](https://scholars.duke.edu/), click "Add Data to My Website", and copy the last part fo the url after `?uri=`.
- All other fields are not required. Click Save.
- The profile's information will be immediately imported from the Scholars website and populate the profile's fields.

#### To add multiple profiles based on the Organization
- Click "Add scholars@duke organization"
- Enter the organization's title, ex: "Sanford School of Public Policy".
- Check "Enabled" and "Import Profiles".
- Add Scholars@Duke Organization URI in the format `https://scholars.duke.edu/individual/org12345678`. To find the organization's URI, please refer to the [Scholars website](https://scholars.duke.edu/).
- Click Save.
- The profiles from this organization will be added during the next import.

### Scholars Fetcher Configuration
To adjust the import/fetching process, go to Configuration -> Web services -> Duke Scholars Fetcher. On the "Settings" tab, the Scholars@duke API database url and maximum caching period of the imported profiles can be changed.

To start the process of importing/fetching the profiles, go to "Import Scholars@Duke data" and click "Import Scholars".

To re-import all existing profiles, check the box to queue all profiles and click "Import Scholars". This will pre-fetch the number of the profiles to be imported. Click "Import Scholars" again to initiate batch importing.

### Editing Scholars Profiles
The list view of the profiles can be sorted by Name, Published/Unpublished status, Active status, Created date, and Updated date.

The information imported from Scholars@Duke cannot be changed in Drupal, but can be viewed in the collapsable region "Imported from Scholars@Duke".

The only field that can be changed is the "Published" checkbox field. Unpublishing a profile will remove it from the listing pages and search.

### Profile fields
The following fields are available for the Scholars Profile and are populated with the incoming data from Scholars@Duke API:
- Name
- Active (boolean field that flags if the profile is enabled in Scholars@Duke)
- Positions (entity reference to the Scholars@Duke Positions custom entity, multiple values)
- Preferred title
- First name, Middle name, Last name, Suffix
- Phone, Email address, Physical address, Mailing address
- Images urls (links to Thumbnail and Portrait images on Scholars@Duke website), Image Alt text
- Scholars@Duke VivoType
- Overview
- Published (can be modified)

The fields can be re-ordered and grouped in the Manage display area.

### Additional fields
If needed, additional fields that be added to the Scholars Profile entity. To add new fields and configure the form and fields display, go to Structure -> Scholars@Duke Profiles -> Manage fields. Follow the normal process of adding new fields to a content type.

### Customizing the template for a Scholars Profile
The default node template for Profile is available and can be themed. To dynamically display information about publications, grants, courses, news feeds, education, professional activities, academic positions, research areas, webpages, artistic events, past appointments, geographical focus, gifts, and licenses, use [Duke Scholars Display module](https://gitlab.oit.duke.edu/dws-contributes/duke-scholars-drupal/drupal8/duke_scholars_display).
