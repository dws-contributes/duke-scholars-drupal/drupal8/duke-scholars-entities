<?php

/**
 * @file
 * Contains duke_scholars_profile_entity.page.inc.
 *
 * Page callback for Scholars@Duke profile entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Scholars@Duke profile templates.
 *
 * Default template: duke-scholars-profile.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_duke_scholars_profile(array &$variables) {
  $duke_scholars_profile_entity = $variables['elements']['#duke_scholars_profile_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

function template_preprocess_duke_scholars_organization(array &$variables) {
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

function template_preprocess_duke_scholars_position(array &$variables) {
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
