<?php

namespace Drupal\duke_scholars_entities;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the scholars@duke profile entity type.
 */
class DukeScholarsEntityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $node, $operation, AccountInterface $account) {
  /** @var \Drupal\node\NodeInterface $node */

    $type = $node->bundle();

    $status = $node->isPublished();

    $mapping = [
      'duke_scholars_profile' => 'profile',
      'duke_scholars_organization' => 'organizations',
      'duke_scholars_position' => 'positions'
    ];

    $mapped = $mapping[$type];

    $admin_permission = $this->entityType->getAdminPermission();
    if ($account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }

    switch ($operation) {
      
      case 'view':
        $perm = "view scholars@duke $mapped";
          return AccessResult::allowedIfHasPermissions($account, [$perm], 'OR');

      case 'update':
        $perm = "edit scholars@duke $mapped";
        return AccessResult::allowedIfHasPermissions($account, [$perm], 'OR');

      case 'delete':
        $perm = "delete scholars@duke $mapped";
        return AccessResult::allowedIfHasPermissions($account, [$perm], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    if ($context && ($context["entity_type_id"] == ('duke_scholars_organization' || 'duke_scholars_profile'))) {
      switch ($context["entity_type_id"]) {
        case 'duke_scholars_organization':
          return AccessResult::allowedIfHasPermissions($account, ['create scholars@duke organizations', 'administer duke_scholars_entities configuration'], 'OR');
        case 'duke_scholars_profile':
          return AccessResult::allowedIfHasPermissions($account, ['create scholars@duke profile', 'administer duke_scholars_entities configuration'], 'OR');
        case 'duke_scholars_position':
          return AccessResult::allowedIfHasPermissions($account, ['create scholars@duke positions', 'administer duke_scholars_entities configuration'], 'OR');
      }
    }
  }
}
