<?php
namespace Drupal\duke_scholars_entities\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Contains the DukeScholarsProfileController controller.
 */
class DukeScholarsProfileController extends ControllerBase
{
 /**
   * The _title_callback for the page that renders a single duke_scholars_profile. Returns the page title.
   *
   * @param \Drupal\Core\Entity\EntityInterface $duke_scholars_profile
   *   The current duke_scholars_profile entity.
   *
   * @return string
   *   The page title.
   */
   public function getTitle() {
      $request = \Drupal::request();
      $page_title = $request->attributes->get('duke_scholars_profile')->getTitle();
      return $page_title;
   }
}
