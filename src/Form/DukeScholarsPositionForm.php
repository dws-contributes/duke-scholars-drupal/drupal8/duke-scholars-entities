<?php

namespace Drupal\duke_scholars_entities\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the scholars@duke positions entity edit forms.
 */
class DukeScholarsPositionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => \Drupal::service('renderer')->render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New scholars@duke positions %label has been created.', $message_arguments));
      $this->logger('duke_scholars_entities')->notice('Created new scholars@duke positions %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The scholars@duke positions %label has been updated.', $message_arguments));
      $this->logger('duke_scholars_entities')->notice('Updated new scholars@duke positions %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.duke_scholars_position.canonical', ['duke_scholars_position' => $entity->id()]);
  }

}
