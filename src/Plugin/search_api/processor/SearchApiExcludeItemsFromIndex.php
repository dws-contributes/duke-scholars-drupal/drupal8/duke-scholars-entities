<?php

namespace Drupal\duke_scholars_entities\Plugin\search_api\processor;

use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Excludes Scholar Profile entities marked as not active from being indexed.
 *
 * @SearchApiProcessor(
 *   id = "search_api_exclude_items_from_index",
 *   label = @Translation("Search API Exclude Items From Index - Custom Processor"),
 *   description = @Translation("Excludes inactive Scholars Profiles from being indexed."),
 *   stages = {
 *     "alter_items" = -50
 *   }
 * )
 */
class SearchApiExcludeItemsFromIndex extends ProcessorPluginBase {

  use PluginFormTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    return $processor;
  }

  /**
   * {@inheritdoc}
   */
  public function alterIndexedItems(array &$items) {

    /** @var \Drupal\search_api\Item\ItemInterface $item */
    foreach ($items as $item_id => $item) {
      $object = $item->getOriginalObject()->getValue();
      $bundle = $object->bundle();

      // Remove Inactive Scholars Profiles from indexed items.
      if ($bundle == 'duke_scholars_profile' && $object->hasField('status')) {
        $value = $object->get('status')->getValue();
        if ($value[0]['value'] == 0) {
          unset($items[$item_id]);
          continue;
        }
      }
    }
  }

}
