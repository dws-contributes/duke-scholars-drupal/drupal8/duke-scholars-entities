<?php

namespace Drupal\duke_scholars_entities\Plugin\Validation\Constraint;

use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ValidUri constraint.
 */
class ValidUriConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {

    $uri = $entity->getScholarsUri();

    $httpClient = \Drupal::httpClient();

    $uriType = duke_scholars_fetcher_uri_type($uri);

    $path = '';

    if ($uriType == 'people') {
      $path = 'endpoints.json';
    } elseif ($uriType == 'organizations') {
      $path = 'people/all.json';
    }

    $api_endpoint = \Drupal::config('duke_scholars_fetcher.settings')->get('duke_scholars_fetcher_widget_base_url');

    try {
      $httpClient->request('GET', $api_endpoint . '/' . $uriType . '/' . $path . '?uri=' . $uri);
    } catch (GuzzleException $e) {
      if ($e->getCode() === 404) {
        $messenger = \Drupal::messenger();
        $messenger->addWarning('Scholar URI does not exist on Scholars@Duke. This Scholar will be unpublished.');
      }
      if ($e->getCode() != 200 && $e->getCode() != 404) {
        $this->context->addViolation($constraint->errorMessage);
      }
    }
  }
}
