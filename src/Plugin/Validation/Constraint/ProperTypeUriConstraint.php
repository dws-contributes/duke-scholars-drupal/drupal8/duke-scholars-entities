<?php

namespace Drupal\duke_scholars_entities\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a ProperTypeUri constraint.
 *
 * @Constraint(
 *   id = "ProperTypeUri",
 *   label = @Translation("ProperTypeUri", context = "Validation"),
 * )
 *
 * @DCG
 * To apply this constraint on a particular field implement
 * hook_entity_type_build().
 */
class ProperTypeUriConstraint extends Constraint {

  public $errorMessage = 'Incorrect URI type.';

}
