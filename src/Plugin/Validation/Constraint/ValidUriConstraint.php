<?php

namespace Drupal\duke_scholars_entities\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides a ValidUri constraint.
 *
 * @Constraint(
 *   id = "ValidUri",
 *   label = @Translation("ValidUri", context = "Validation"),
 * )
 *
 * @DCG
 * To apply this constraint on a particular field implement
 * hook_entity_type_build().
 */
class ValidUriConstraint extends Constraint {

  public $errorMessage = 'Unable to fetch Scholars@Duke data';

}
