<?php

namespace Drupal\duke_scholars_entities\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the ProperTypeUri constraint.
 */
class ProperTypeUriConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {

    $uri = $entity->getScholarsUri();

    $typeMap = [
      "people" => "duke_scholars_profile",
      "organizations" => "duke_scholars_organization"
    ];

    $uriType = FALSE;
    $uriParts = explode('/', $uri);
    $bareUri = end($uriParts);
    if (strpos($bareUri, 'per') === 0) {
      $uriType = 'people';
    } elseif (strpos($bareUri, 'org') === 0) {
      $uriType = 'organizations';
    }

    $entityType = $entity->getEntityTypeId();

    if ( !($uriType) || (!key_exists($uriType, $typeMap) && ($typeMap[$uriType] != $entityType)) ) {
      $this->context->addViolation($constraint->errorMessage);
    }
  }
}
