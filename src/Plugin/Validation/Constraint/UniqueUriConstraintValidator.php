<?php

namespace Drupal\duke_scholars_entities\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Validates the UniqueUriConstraint constraint.
 */
class UniqueUriConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    
    // if there is an ID, we are updating an existing entity
    $entityID = $entity->id();
    if ($entityID) {
      return;
    }
    
    $uri = $entity->getScholarsUri();

    $entity_type_id = $entity->getEntityTypeId();

    $value_exists = (bool) \Drupal::entityQuery($entity_type_id)
      ->accessCheck(TRUE)
      ->condition('duke_scholars_uri', $uri)
      ->range(0,1)
      ->count()
      ->execute();

    if ($value_exists) {
      $this->context->addViolation($constraint->errorMessage);
    }

  }

}
