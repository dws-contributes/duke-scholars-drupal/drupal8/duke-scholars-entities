<?php

namespace Drupal\duke_scholars_entities\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an UniqueUriConstraint constraint.
 *
 * @Constraint(
 *   id = "UniqueURI",
 *   label = @Translation("UniqueUriConstraint", context = "Validation"),
 * )
 *
 * @DCG
 * To apply this constraint on a particular field implement
 * hook_entity_type_build().
 */
class UniqueUriConstraint extends Constraint {

  public $errorMessage = 'This URI is already in the system';

}
