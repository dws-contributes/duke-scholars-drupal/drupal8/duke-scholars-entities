<?php

namespace Drupal\duke_scholars_entities\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'duke_scholars_profile_images_default' formatter.
 *
 * @FieldFormatter(
 *   id = "duke_scholars_profile_images_thumbnail",
 *   label = @Translation("Thumbnail"),
 *   field_types = {"duke_scholars_profile_images"}
 * )
 */
class ImagesThumbnailFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      if ($item->thumbnail) {
        $elements[$delta] = $this->viewValue($item);
      }
    }
    return $elements;
  }

  /**
   * generate output for a single item
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *
   * @return array
   */
  protected function viewValue(FieldItemInterface $item) {
    $url = $item->get('thumbnail')->getValue();
    $alt = $item->get('alt_text')->getValue();
    return [
      '#theme' => 'duke_scholars_profile_thumbnail',
      '#url' => $url,
      '#alt' => $alt
    ];
  }
}
