<?php

namespace Drupal\duke_scholars_entities\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'duke_scholars_profile_images_profile' formatter.
 *
 * @FieldFormatter(
 *   id = "duke_scholars_profile_images_profile",
 *   label = @Translation("Profile"),
 *   field_types = {"duke_scholars_profile_images"}
 * )
 */
class ImagesProfileFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  protected function viewValue(FieldItemInterface $item) {
    $url = $item->get('portrait')->getValue();
    $alt = $item->get('alt_text')->getValue();
    return [
      '#theme' => 'duke_scholars_profile_portrait',
      '#url' => $url,
      '#alt' => $alt
    ];
  }

}
