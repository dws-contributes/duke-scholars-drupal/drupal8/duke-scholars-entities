<?php

namespace Drupal\duke_scholars_entities\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'duke_scholars_profile_name_default' formatter.
 *
 * @FieldFormatter(
 *   id = "duke_scholars_profile_name_default",
 *   label = @Translation("Default"),
 *   field_types = {"duke_scholars_profile_name"}
 * )
 */
class NameDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['foo' => 'bar'] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $element['foo'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Foo'),
      '#default_value' => $settings['foo'],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary[] = $this->t('Foo: @foo', ['@foo' => $settings['foo']]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      if ($item->first_name) {
        $element[$delta]['first_name'] = [
          '#type' => 'item',
          '#title' => $this->t('First Name'),
          '#markup' => $item->first_name,
        ];
      }

      if ($item->last_name) {
        $element[$delta]['last_name'] = [
          '#type' => 'item',
          '#title' => $this->t('Last Name'),
          '#markup' => $item->last_name,
        ];
      }

      if ($item->suffix) {
        $element[$delta]['suffix'] = [
          '#type' => 'item',
          '#title' => $this->t('Suffix'),
          '#markup' => $item->suffix,
        ];
      }

    }

    return $element;
  }

}
