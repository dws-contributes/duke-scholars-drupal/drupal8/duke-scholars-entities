<?php

namespace Drupal\duke_scholars_entities\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'duke_scholars_uri_default' formatter.
 *
 * @FieldFormatter(
 *   id = "duke_scholars_uri_default",
 *   label = @Translation("Default"),
 *   field_types = {"duke_scholars_uri"}
 * )
 */
class ScholarsDukeUriDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {

      if ($item->uri) {
        $element[$delta]['uri'] = [
          '#type' => 'item',
          '#title' => $this->t('URI'),
          '#markup' => $item->uri,
        ];
      }

    }

    return $element;
  }

}
