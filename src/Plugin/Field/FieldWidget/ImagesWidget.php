<?php

namespace Drupal\duke_scholars_entities\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'duke_scholars_profile_images' field widget.
 *
 * @FieldWidget(
 *   id = "duke_scholars_profile_images",
 *   label = @Translation("Images"),
 *   field_types = {"duke_scholars_profile_images"},
 * )
 */
class ImagesWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'fieldset_state' => 'closed'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['fieldset_state'] = [
      '#type' => 'select',
      '#title' => t('Fieldset default state'),
      '#options' => [
        'open' => t('Open'),
        'closed' => t('Closed')
      ],
      '#default_value' => $this->getSetting('fieldset_state'),
      '#description' => t('The default state of the fieldset that contains the image field values')
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('fieldset is @fieldset_state', ['@fieldset_state' => $this->getSetting('fieldset_state')]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['images'] = [
      '#type' => 'details',
      '#title' => $element['#title'],
      '#open' => $this->getSetting('fieldset_state') == 'open' ? TRUE : FALSE,
      // '#description' => $element['#description'],
    ] + $element;

    $element['images']['thumbnail'] = [
      '#type' => 'url',
      '#title' => $this->t('Thumbnail'),
      '#default_value' => isset($items[$delta]->thumbnail) ? $items[$delta]->thumbnail : NULL,
      '#size' => 100,
    ];

    $element['images']['portrait'] = [
      '#type' => 'url',
      '#title' => $this->t('Portrait'),
      '#default_value' => isset($items[$delta]->portrait) ? $items[$delta]->portrait : NULL,
      '#size' => 100,
    ];

    $element['images']['alt_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alt Text'),
      '#default_value' => isset($items[$delta]->alt_text) ? $items[$delta]->alt_text : NULL,
      '#size' => 100,
    ];

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'duke-scholars-profile-images-elements';
    $element['#attached']['library'][] = 'duke_scholars_profile/duke_scholars_profile_images';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      if (!isset($value['thumbnail']) || ($value['thumbnail'] === '')) {
        $values[$delta]['thumbnail'] = NULL;
      }
      if (!isset($value['portrait']) || ($value['portrait'] === '')) {
        $values[$delta]['portrait'] = NULL;
      }
      if (!isset($value['alt_text']) || ($value['alt_text'] === '')) {
        $values[$delta]['alt_text'] = NULL;
      }
    }
    return $values;
  }

}
