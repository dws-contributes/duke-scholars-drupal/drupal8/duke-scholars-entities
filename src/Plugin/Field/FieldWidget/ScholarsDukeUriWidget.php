<?php

namespace Drupal\duke_scholars_entities\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Defines the 'duke_scholars_uri' field widget.
 *
 * @FieldWidget(
 *   id = "duke_scholars_uri",
 *   label = @Translation("Scholars@Duke URI"),
 *   field_types = {"duke_scholars_uri"},
 * )
 */
class ScholarsDukeUriWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $element['uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URI'),
      '#default_value' => isset($items[$delta]->uri) ? $items[$delta]->uri : NULL,
      '#size' => 60,
    ];

    $element['#theme_wrappers'] = ['container', 'form_element'];
    $element['#attributes']['class'][] = 'container-inline';
    $element['#attributes']['class'][] = 'duke-scholars-uri-elements';
    $element['#attached']['library'][] = 'duke_scholars_entities/duke_scholars_uri';

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
    return isset($violation->arrayPropertyPath[0]) ? $element[$violation->arrayPropertyPath[0]] : $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      if ($value['uri'] === '') {
        $values[$delta]['uri'] = NULL;
      }
    }
    return $values;
  }

}
