<?php

namespace Drupal\duke_scholars_entities\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'duke_scholars_profile_name' field type.
 *
 * @FieldType(
 *   id = "duke_scholars_profile_name",
 *   label = @Translation("Name"),
 *   category = @Translation("General"),
 *   default_widget = "duke_scholars_profile_name",
 *   default_formatter = "duke_scholars_profile_name_default"
 * )
 */
class NameItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if ($this->first_name !== NULL) {
      return FALSE;
    }
    elseif ($this->last_name !== NULL) {
      return FALSE;
    }
    elseif ($this->suffix !== NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['first_name'] = DataDefinition::create('string')
      ->setLabel(t('First Name'));
    $properties['last_name'] = DataDefinition::create('string')
      ->setLabel(t('Last Name'));
    $properties['suffix'] = DataDefinition::create('string')
      ->setLabel(t('Suffix'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    // @todo Add more constrains here.
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      'first_name' => [
        'type' => 'varchar',
        'length' => 255,
      ],
      'last_name' => [
        'type' => 'varchar',
        'length' => 255,
      ],
      'suffix' => [
        'type' => 'varchar',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
      // @DCG Add indexes here if necessary.
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {

    $random = new Random();

    $values['first_name'] = $random->word(mt_rand(1, 255));

    $values['last_name'] = $random->word(mt_rand(1, 255));

    $values['suffix'] = $random->word(mt_rand(1, 255));

    return $values;
  }

}
