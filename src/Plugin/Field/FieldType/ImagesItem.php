<?php

namespace Drupal\duke_scholars_entities\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'duke_scholars_profile_images' field type.
 *
 * @FieldType(
 *   id = "duke_scholars_profile_images",
 *   label = @Translation("Images"),
 *   category = @Translation("General"),
 *   default_widget = "duke_scholars_profile_images",
 *   default_formatter = "duke_scholars_profile_images_profile"
 * )
 */
class ImagesItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if ($this->thumbnail !== NULL) {
      return FALSE;
    }
    elseif ($this->portrait !== NULL) {
      return FALSE;
    }
    elseif ($this->alt_text !== NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['thumbnail'] = DataDefinition::create('uri')
      ->setLabel(t('Thumbnail'));
    $properties['portrait'] = DataDefinition::create('uri')
      ->setLabel(t('Portrait'));
    $properties['alt_text'] = DataDefinition::create('string')
      ->setLabel(t('Alt Text'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    // @todo Add more constrains here.
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {

    $columns = [
      'thumbnail' => [
        'type' => 'varchar',
        'length' => 2048,
      ],
      'portrait' => [
        'type' => 'varchar',
        'length' => 2048,
      ],
      'alt_text' => [
        'type' => 'varchar',
        'length' => 255,
      ],
    ];

    $schema = [
      'columns' => $columns,
      // @DCG Add indexes here if necessary.
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {

    $random = new Random();

    $tlds = ['com', 'net', 'gov', 'org', 'edu', 'biz', 'info'];
    $domain_length = mt_rand(7, 15);
    $protocol = mt_rand(0, 1) ? 'https' : 'http';
    $www = mt_rand(0, 1) ? 'www' : '';
    $domain = $random->word($domain_length);
    $tld = $tlds[mt_rand(0, (count($tlds) - 1))];
    $values['thumbnail'] = "$protocol://$www.$domain.$tld";

    $tlds = ['com', 'net', 'gov', 'org', 'edu', 'biz', 'info'];
    $domain_length = mt_rand(7, 15);
    $protocol = mt_rand(0, 1) ? 'https' : 'http';
    $www = mt_rand(0, 1) ? 'www' : '';
    $domain = $random->word($domain_length);
    $tld = $tlds[mt_rand(0, (count($tlds) - 1))];
    $values['portrait'] = "$protocol://$www.$domain.$tld";

    $values['alt_text'] = $random->word(mt_rand(1, 255));

    return $values;
  }

}
