<?php

namespace Drupal\duke_scholars_entities;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a scholars@duke profile entity type.
 */
interface DukeScholarsEntityInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the scholars@duke profile title.
   *
   * @return string
   *   Title of the scholars@duke profile.
   */
  public function getTitle();

  /**
   * Sets the scholars@duke profile title.
   *
   * @param string $title
   *   The scholars@duke profile title.
   *
   * @return \Drupal\duke_scholars_entities\DukeScholarsProfileInterface
   *   The called scholars@duke profile entity.
   */
  public function setTitle($title);

  /**
   * Gets the scholars@duke profile creation timestamp.
   *
   * @return int
   *   Creation timestamp of the scholars@duke profile.
   */
  public function getCreatedTime();

  /**
   * Sets the scholars@duke profile creation timestamp.
   *
   * @param int $timestamp
   *   The scholars@duke profile creation timestamp.
   *
   * @return \Drupal\duke_scholars_profile\DukeScholarsProfileInterface
   *   The called scholars@duke profile entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the scholars@duke profile active status.
   *
   * @return bool
   *   TRUE if the scholars@duke profile is active, FALSE otherwise.
   */
  public function isActive();

  /**
   * Sets the scholars@duke profile active status.
   *
   * @param bool $active
   *   TRUE to enable this scholars@duke profile, FALSE to disable.
   *
   * @return \Drupal\duke_scholars_profile\DukeScholarsProfileInterface
   *   The called scholars@duke profile entity.
   */
  public function setActive($active);

/**
   * Returns the scholars@duke profile published status.
   *
   * @return bool
   *   TRUE if the scholars@duke profile is published, FALSE otherwise.
   */
  public function isPublished();


  /**
   * Sets the entity as published.
   *
   * @return $this
   *
   */
  public function setPublished();

  /**
   * Sets the entity as unpublished.
   *
   * @return $this
   *
   */
  public function setUnpublished();

  /**
   * Gets the scholars@duke profile last modified timestamp, which is pulled in
   * via the Scholars@Duke api.
   *
   * @return int
   */
  public function getLastModified();

  public function setLastModified($updatedAt);

  /**
   * Gets the scholars@duke URI
   *
   * @return string
   */
  public function getScholarsUri();


  /**
   * Sets the scholars@duke URI
   *
   * @param string $uri
   *
   * @return mixed
   */
  public function setScholarsUri($uri);

}
