<?php

namespace Drupal\duke_scholars_entities\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\duke_scholars_entities\DukeScholarsEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the scholars@duke profile entity class.
 *
 * @ContentEntityType(
 *   id = "duke_scholars_profile",
 *   label = @Translation("Scholars@Duke Profile"),
 *   label_collection = @Translation("Scholars@Duke Profiles"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\duke_scholars_entities\DukeScholarsEntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\duke_scholars_entities\DukeScholarsEntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\duke_scholars_entities\Form\DukeScholarsProfileForm",
 *       "edit" = "Drupal\duke_scholars_entities\Form\DukeScholarsProfileForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "duke_scholars_profile",
 *   revision_table = "duke_scholars_profile_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer duke_scholars_entities configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "published" = "status",
 *     "revision" = "revision_id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/duke-scholars-profile/add",
 *     "canonical" = "/duke_scholars_profile/{duke_scholars_profile}",
 *     "edit-form" = "/admin/content/duke-scholars-profile/{duke_scholars_profile}/edit",
 *     "delete-form" = "/admin/content/duke-scholars-profile/{duke_scholars_profile}/delete",
 *     "collection" = "/admin/content/duke-scholars-profile"
 *   },
 *   constraints = {
 *    "UniqueURI" = {},
 *    "ProperTypeUri" = {},
 *    "ValidUri" = {}
 *   },
 *   field_ui_base_route = "duke_scholars_entities.duke_scholars_profile.settings"
 * )
 */
class DukeScholarsProfile extends RevisionableContentEntityBase implements DukeScholarsEntityInterface, EntityPublishedInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  protected $EntityTypeID = 'duke_scholars_profile';

  /**
   * {@inheritdoc}
   *
   * When a new scholars@duke profile entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return (bool) $this->get('active')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($active) {
    $this->set('active', $active);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished() {
    $this->set('status', TRUE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setUnpublished() {
    $this->set('status', FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Name'))
      ->setDescription(t('Name will be automatically updated to reflect information from Scholars@Duke.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Active'))
      ->setDefaultValue(TRUE)
      ->setReadOnly(TRUE)
      ->setSetting('on_label', 'Active')
      ->setSetting('off_label', 'Inactive')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Published'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Published')
      ->setSetting('off_label', 'Unpublished')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_uri'] = BaseFieldDefinition::create('duke_scholars_uri')
      ->setRevisionable(TRUE)
      ->setLabel(t('Scholars@Duke URI'))
      ->setDescription(t('The URI of the individual in Scholars@Duke (e.g. https://scholars.duke.edu/individual/per12345678).'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'duke_scholars_uri',
        'weight' => 5
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'duke_scholars_uri',
        'weight' => 5
      ]);

    $fields['duke_scholars_profile_name_first'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('First Name'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_profile_name_middle'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Middle Name'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_profile_name_last'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Last Name'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_profile_name_suffix'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Suffix'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_profile_thumbnail'] = BaseFieldDefinition::create('duke_scholars_profile_images')
      ->setRevisionable(TRUE)
      ->setLabel(t('Images'))
      ->setDescription(t('Links to images in Scholars@Duke.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'duke_scholars_profile_images_profile',
        'weight' => 15
      ])
      ->setDisplayOptions('form', [
        'type' => 'duke_scholars_profile_images',
        'weight' => 15
      ]);

    $fields['duke_scholars_profile_overview'] = BaseFieldDefinition::create('text_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Overview'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'weight' => 20
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 20
      ]);

    $fields['duke_scholars_profile_additional_information'] = BaseFieldDefinition::create('text_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Additional Information'))
      ->setDescription(t('Additional content for this profile. This field is not affected by the Scholars@Duke data import.'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'weight' => 20
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 20
      ]);

    $fields['duke_scholars_profile_phone'] = BaseFieldDefinition::create('telephone')
      ->setRevisionable(TRUE)
      ->setLabel(t('Phone'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'telephone_link',
        'weight' => 25
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'telephone_default',
        'weight' => 25
      ]);

   $fields['duke_scholars_profile_address_1'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Physical address'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('view', TRUE);

   $fields['duke_scholars_profile_address_2'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Mailing address'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 31
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => 31
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_profile_email'] = BaseFieldDefinition::create('email')
      ->setRevisionable(TRUE)
      ->setLabel(t('Email address'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'email_default',
        'weight' => 32
      ])
      ->setDisplayOptions('view', [
        'type' => 'email_mailto',
        'weight' => 32
      ]);

    $fields['duke_scholars_profile_last_modified'] = BaseFieldDefinition::create('datetime')
      ->setRevisionable(TRUE)
      ->setLabel(t('Last modified date/time in Scholars@Duke'))
      ->setDisplayOptions('view', [
        'type' => 'datetime_default',
        'weight' => 35
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_default',
        'weight' => 35
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['duke_scholars_profile_preferred_title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Preferred Title'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 40,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 40,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_profile_positions'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Positions'))
      ->setCardinality(-1)
      ->setSetting('target_type', 'duke_scholars_position')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 42,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 42,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_vivotype'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Scholars@Duke VivoType'))
      ->setCardinality(1)
      ->setSettings([
          'target_type' => 'taxonomy_term',
          'target_bundle' => 'duke_scholars_vivotypes',
          'auto_create' => TRUE
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'taxonomy_term',
        'weight' => 45
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 45
      ]);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 50,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 50,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 55,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 55,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDisplayOptions('form', [
        'weight' => 60,
        'type' => 'datetime_timestamp'
      ])
      ->setDisplayOptions('view', [
        'weight' => 60,
        'type' => 'timestamp',
        'label' => 'above'
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Calling $fields = parent::baseFieldDefinitions($entity_type); will add
    // Langcode for us. Here we want to set a default to the entity so we don't
    // need to explicitly set the field value during inital entity creation.
    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setName('langcode')
      ->setDefaultValue('x-default')// x-default is the sites default language.
      ->setStorageRequired(TRUE)
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code.'))
      ->setTranslatable(TRUE);

    // Add the default_langcode, this depicts if the content is in it default translation, IE: should be true for original content and false on any translation.
    // Note that making this field translatable is important, if you do not you will run into issues overwriting your translation sources.
    $fields['default_langcode'] = BaseFieldDefinition::create('boolean')
      ->setName('default_langcode')
      ->setLabel(t('Default Language code'))
      ->setDescription(t('Indicates if this is the default language.'))
      ->setDefaultValue(1) // Default this to 1.
      ->setTargetEntityTypeId('duke_scholars_profile')
      ->setTargetBundle(NULL)
      ->setStorageRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * @inheritDoc
   */
  public function getLastModified() {
    return $this->get('duke_scholars_profile_last_modified')->value;
  }

  /**
   * @inheritDoc
   */
  public function getScholarsUri() {
    $uriValue = $this->get('duke_scholars_uri')->getValue();
    return $uriValue[0]["uri"] ?? '0';
  }

  /**
   * @inheritDoc
   */
  public function setScholarsUri($uri) {
    // TODO: Implement setScholarsUri() method.
  }

  public function setLastModified($updatedAt) {
    $date = date('Y-m-d\TH:i:s', strtotime($updatedAt));
    $this->set('duke_scholars_profile_last_modified', $date);
  }
}
