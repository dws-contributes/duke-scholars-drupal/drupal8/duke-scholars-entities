<?php

namespace Drupal\duke_scholars_entities\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\duke_scholars_entities\DukeScholarsEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the scholars@duke organizations entity class.
 *
 * @ContentEntityType(
 *   id = "duke_scholars_organization",
 *   label = @Translation("Scholars@Duke Organizations"),
 *   label_collection = @Translation("Scholars@Duke Organizations"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\duke_scholars_entities\DukeScholarsEntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\duke_scholars_entities\DukeScholarsEntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\duke_scholars_entities\Form\DukeScholarsOrganizationForm",
 *       "edit" = "Drupal\duke_scholars_entities\Form\DukeScholarsOrganizationForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "duke_scholars_organization",
 *   revision_table = "duke_scholars_organization_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer duke_scholars_entities configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "published" = "status",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/duke-scholars-organizations/add",
 *     "canonical" = "/duke_scholars_organization/{duke_scholars_organization}",
 *     "edit-form" = "/admin/content/duke-scholars-organizations/{duke_scholars_organization}/edit",
 *     "delete-form" = "/admin/content/duke-scholars-organizations/{duke_scholars_organization}/delete",
 *     "collection" = "/admin/content/duke-scholars-organizations"
 *   },
 *   constraints = {
 *    "ProperTypeUri" = {},
 *    "ValidUri" = {},
 *    "UniqueURI" = {}
 *   },
 *   field_ui_base_route = "duke_scholars_entities.duke_scholars_organization.settings"
 * )
 */
class DukeScholarsOrganization extends RevisionableContentEntityBase implements DukeScholarsEntityInterface, EntityPublishedInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new scholars@duke organizations entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return (bool) $this->get('active')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setActive($active) {
    $this->set('active', $active);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  public function getDisplayName() {
    return $this->label();
  }

  public function getAccountName() {
    $uid = $this->get('uid')->getValue();
    if ($uid) {
      return $this->getOwner()->label();
    } else {
      return 'anonymous';
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the scholars@duke organization.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['active'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Active'))
      ->setDescription(t('is this organization active?'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Published'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Published')
      ->setSetting('off_label', 'Unpublished')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['import_associated_people'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Import People'))
      ->setDescription(t('Check this box to import all of the associated profiles for this organization'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Import Profiles')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 5,
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 5,
      ])
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 5,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['duke_scholars_uri'] = BaseFieldDefinition::create('duke_scholars_uri')
      ->setLabel(t('Scholars@Duke URI'))
      ->setDescription(t('The URI of the organization in Scholars@Duke (e.g. https://scholars.duke.edu/individual/org12345678)'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'duke_scholars_uri',
        'weight' => 10
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'duke_scholars_uri',
        'weight' => 10
      ]);

    $fields['parent_org'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setRequired(FALSE)
      ->setLabel(t('Parent Organization'))
      ->setDescription(t('This organization`s parent organization'))
      ->setSetting('target_type', 'duke_scholars_organization')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('the author of this scholars@duke organization'))
      ->setSetting('target_type', 'user')
      ->setDefaultValue(1)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 20,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the scholars@duke organization was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 30,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the scholars@duke organization was last edited.'))
      ->setDisplayOptions('form', [
        'weight' => 35
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['duke_scholars_organization_person_uris'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Person URIs'))
      ->setCardinality(-1)
      ->setDescription(t('The list of Person URIs affiliated with this organization'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string',
        'weight' => 40
      ]);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setName('langcode')
      ->setDefaultValue('x-default')// x-default is the sites default language.
      ->setStorageRequired(TRUE)
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code.'))
      ->setTranslatable(TRUE);

    $fields['default_langcode'] = BaseFieldDefinition::create('boolean')
      ->setName('default_langcode')
      ->setLabel(t('Default Language code'))
      ->setDescription(t('Indicates if this is the default language.'))
      ->setDefaultValue(1) // Default this to 1.
      ->setTargetEntityTypeId('duke_scholars_organization')
      ->setTargetBundle(NULL)
      ->setStorageRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);


    return $fields;
  }

  /**
   * @inheritDoc
   */
  public function getScholarsUri() {
    return $this->get('duke_scholars_uri')->getString();
  }

  public function setScholarsUri($uri) {
    $this->set('duke_scholars_uri', $uri);
  }

  public function getLastModified() {
    // TODO: Implement getLastModified() method.
    return $this->get('updated')->getString();
  }

  public function setLastModified($updatedAt) {
    // TODO: Implement setLastModified() method.
    $this->set('updated', $updatedAt);
  }
}
