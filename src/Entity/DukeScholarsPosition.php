<?php

namespace Drupal\duke_scholars_entities\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\duke_scholars_entities\DukeScholarsEntityInterface;
use Drupal\user\UserInterface;

/**
 * Defines the scholars@duke positions entity class.
 *
 * @ContentEntityType(
 *   id = "duke_scholars_position",
 *   label = @Translation("Scholars@Duke Positions"),
 *   label_collection = @Translation("Scholars@Duke Positions"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\duke_scholars_entities\DukeScholarsEntityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\duke_scholars_entities\DukeScholarsEntityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\duke_scholars_entities\Form\DukeScholarsPositionForm",
 *       "edit" = "Drupal\duke_scholars_entities\Form\DukeScholarsPositionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "duke_scholars_position",
 *   revision_table = "duke_scholars_position_revision",
 *   admin_permission = "administer duke_scholars_entities configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/content/duke-scholars-position/add",
 *     "canonical" = "/duke_scholars_position/{duke_scholars_position}",
 *     "edit-form" = "/admin/content/duke-scholars-position/{duke_scholars_position}/edit",
 *     "delete-form" = "/admin/content/duke-scholars-position/{duke_scholars_position}/delete",
 *     "collection" = "/admin/content/duke-scholars-position"
 *   },
 *   constraints = {
 *    "UniqueURI" = {},
 *   },
 *   field_ui_base_route = "duke_scholars_entities.duke_scholars_position.settings"
 * )
 */
class DukeScholarsPosition extends ContentEntityBase implements DukeScholarsEntityInterface, EntityPublishedInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new scholars@duke positions entity is created, set the uid entity reference to
   * the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  public function setPreferred($preferred) {
    $this->set('position_preferred', (bool) $preferred);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the scholars@duke positions entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the scholars@duke positions author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the scholars@duke positions was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the scholars@duke positions was last edited.'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the scholars@duke profile author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['position_uri'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Position URI'))
      ->setDescription(t('The URI for this position in Scholars@Duke'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ]);

    $fields['parent_org'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setRequired(FALSE)
      ->setLabel(t('Position Organization'))
      ->setDescription(t('This position`s organization'))
      ->setSetting('target_type', 'duke_scholars_organization')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['position_vivo_type'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Scholars@Duke VivoType'))
      ->setCardinality(1)
      ->setDescription(t('the type of data, as stored in Scholars@Duke'))
      ->setSettings([
          'target_type' => 'taxonomy_term',
          'target_bundle' => 'duke_scholars_vivotypes',
          'auto_create' => TRUE
        ]
      )
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'taxonomy_term',
        'weight' => 80
      ])
      ->setDisplayOptions('form', [
        'type' => 'taxonomy_term',
        'weight' => 100
      ]);

      $fields['position_preferred'] = BaseFieldDefinition::create('boolean')
        ->setDefaultValue(FALSE)
        ->setLabel(t('Preferred'))
        ->setDescription(t('Is this the preferred title?'))
        ->setSetting('on_label', 'Preferred')
        ->setDisplayOptions('form', [
          'type' => 'boolean_checkbox',
          'settings' => [
            'display_label' => FALSE,
          ],
          'weight' => 0,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'type' => 'boolean',
          'label' => 'above',
          'weight' => 0,
          'settings' => [
            'format' => 'enabled-disabled',
          ],
        ])
        ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  public function isActive() {
    // TODO: Implement isEnabled() method.
  }

  public function setActive($active) {
    // TODO: Implement setStatus() method.
  }

  public function getLastModified() {
    // TODO: Implement getLastModified() method.
  }

  public function setLastModified($updatedAt) {
    // TODO: Implement setLastModified() method.
  }

  /**
   * @inheritDoc
   */
  public function getScholarsUri() {
    // TODO: Implement getScholarsUri() method.
  }

  /**
   * @inheritDoc
   */
  public function setScholarsUri($uri) {
    // TODO: Implement setScholarsUri() method.
  }

}
